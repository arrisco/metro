var Metro = {};

$(window).on('load', function(){
	Metro.Carrossel.init();
});

Metro.Carrossel = {
	init : function(){
		var that = this;

		that.primeiraImagem();
	},
	primeiraImagem : function(){
		var that = this;
		var primeiraImagem    = $('.metroItem img').eq(0);
		var primeiraImagemSrc = primeiraImagem.attr('src');

		var img = new Image;
		img.src = primeiraImagemSrc;

		img.onload = function(){
			that.animar(primeiraImagem);
		};

	},
	animar : function(imagem){
		that = this;
		that.larguraImagem = parseFloat(imagem.outerWidth(), 10);

		var daEsquerda = that.larguraImagem * -1;

		$('.metroFilme').animate({'left' : daEsquerda}, 2000, function(){
			var primeira = $('.metroItem').eq(0);
			primeira.remove();
			$('.metroFilme').append(primeira);
			$('.metroFilme').css('left', 0);
			that.primeiraImagem();
		});

	},
	larguraPrimeiraImagem : 0
};
